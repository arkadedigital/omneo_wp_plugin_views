<?php
global $CONTENT_TYPE_LIST;
$content_type = $CONTENT_TYPE_LIST[get_field('content_type', $_GET['post'])];
$content_item_id = $content_type ? \Omneo\Content\get_content_item_id(get_field('omneo_id', $_GET['post']), $content_type) : '';
?>


<div id="ng-views">
    <p class="description">Add the "Views" where this item will be displayed. Multiple views can be assigned per
        item.</p>

    <div ng-controller="ViewsCtrl" ng-cloak ng-init="content_item_id='<?php echo $content_item_id ?>'; content_type='<?php echo $content_type; ?>'">
        <?php $query = new \WP_Query(['post_type' => 'views', 'post_status' => 'publish']); ?>

        <table style="width:100%;" ng-show="is_loaded">
            <tr>
                <td>
                    <select style="width:100%;" ng-model="selected_view">
                        <option value=""></option>
                        <?php if ($query->have_posts()): $index = 0; ?>
                            <?php while ($query->have_posts()): $query->the_post(); ?>
                                <?php if ($omneo_id = get_field('omneo_id', get_the_ID())): ?>
                                    <option value="<?php echo $omneo_id; ?>"
                                            ng-init="views[<?php echo $index; ?>]['id']=<?php echo $omneo_id; ?>;views[<?php echo $index; ?>]['title']='<?php the_title(); ?>'">
                                        #<?php echo $omneo_id; ?> - <?php the_title(); ?>
                                    </option>
                                    <?php $index++; endif; ?>
                            <?php endwhile; ?>
                        <?php endif;
                        wp_reset_postdata(); ?>
                    </select>
                </td>
                <td>
                    <button type="button" class="button" ng-disabled="!selected_view"
                            ng-click="add_selected_view(selected_view)">+
                    </button>
                </td>
            </tr>
        </table>


        <table ng-if="selected_views.length>0">
            <tr ng-repeat="view in selected_views" ng-show="view.action !== 'delete'">
                <td>
                    #{{ view.view_id }} {{ view.title }}
                </td>
                <td>
                    <div class="dashicons dashicons-dismiss" ng-click="delete_view(view)"></div>
                    <input type="hidden" name="views[{{ $index }}][action]" value="{{ view.action }}"
                           ng-disabled="!view.action">
                    <input type="hidden" name="views[{{ $index }}][id]" value="{{ view.id }}"
                           ng-disabled="!view.action || content_type==='lookbooks' || content_type==='gallery'">
                    <input type="hidden" name="views[{{ $index }}][id]" value="{{ view.view_id }}"
                           ng-disabled="content_type!=='lookbooks' && content_type!=='gallery'">
                </td>
            </tr>
        </table>

        <div ng-if="selected_views.length===0 && is_loaded">No views</div>
        <div ng-if="!is_loaded"><span class="spinner is-active" style="float:none;margin-top:0;"></span> Loading
            views...
        </div>
    </div>
</div>


