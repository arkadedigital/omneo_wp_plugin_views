<?php namespace Omneo\Views\PostTypes;

defined('ABSPATH') or die('Access Denied');


function omneo_view_post_type()
{

    register_post_type('views', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
        array('labels' => array(
            'name' => __('Views'), /* This is the Title of the Group */
            'singular_name' => __('View'), /* This is the individual type */
            'all_items' => __('All Views'), /* the all items menu item */
            'add_new' => __('Add New'), /* The add new menu item */
            'add_new_item' => __('Add New View'), /* Add New Display Title */
            'edit' => __('Edit'), /* Edit Dialog */
            'edit_item' => __('Edit Views'), /* Edit Display Title */
            'new_item' => __('New View'), /* New Display Title */
            'view_item' => __('View the View'), /* View Display Title */
            'search_items' => __('Search View'), /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ), /* end of arrays */
            'description' => __('This is the main "Views" section containing the content items on the App'), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => null, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon' => 'dashicons-visibility', /* the icon for the view type menu */
            'rewrite' => array('slug' => 'views', 'with_front' => false), /* you can specify its url slug */
            'has_archive' => 'views', /* you can rename the slug here */
            'capability_type' => 'post',
            'hierarchical' => false,
            /* the next one is important, it tells what's enabled in the post editor */
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'sticky', 'page-attributes')
        )
    );
}

add_action('init', __NAMESPACE__ . '\\omneo_view_post_type');