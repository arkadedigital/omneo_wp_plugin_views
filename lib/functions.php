<?php namespace Omneo\Views;

use Omneo\Core;

defined('ABSPATH') or die('Access Denied');


/**
 * On saving post (create or edit) make api call to Omneo
 *
 * @param $post_id
 * @param $post
 * @param $updated
 */
function save_post($post_id, $post, $updated)
{

    if ($post->post_type == 'views' && $updated && $post->post_status == 'publish') {
        $omneo_id = get_field('omneo_id', $post_id);
        $description = get_field('description', $post_id);
        $icon_on = get_field('icon_on', $post_id);
        $icon_off = get_field('icon_off', $post_id);
        $system_view = get_field('system_view', $post_id);

        // Data
        $args['data'] = [
            // Required
            'title' => $post->post_title,
            // Optional
            'description' => $description,
            'icon_on_image_url' => isset($icon_on['url']) ? $icon_on['url'] : '',
            'icon_off_image_url' => isset($icon_off['url']) ? $icon_off['url'] : '',
            'display_in_menu' => $system_view
        ];

        $args['api_request'] = 'content/' . ($omneo_id ? 'views/' . $omneo_id : 'views');
        $args['verb'] = $omneo_id ? 'put' : 'post';

        // Make request
        $response = Core\send_request($args);

        // API Error
        if (isset($response['error']) || !isset($response['data'])) {
            // Change post status to draft
            global $wpdb;
            $wpdb->update('wp_posts', ['post_status' => 'draft'], ['ID' => $args['post_id']]);

            if (isset($response['error']['message'])) {
                add_action('admin_notices', function ($response) {
                    echo "<div class='error'> <p>" . $response['error']['message'] . "</p></div>";
                });

                die($response['error']['message']);
            } else {
                die('Omneo API Error');
            }

        } elseif (!$omneo_id) { // Create
            // Update omneo id
            update_field('field_5614b481eba73', $response['data']['id'], $args['post_id']);

        }
            update_field('field_56789f5076aee', $response['data']['content_item_id'], $args['post_id']);
    }
}

add_action('save_post', __NAMESPACE__ . '\\save_post', 10, 3);

/**
 * Delete view
 *
 * @param $post_id
 */
add_action('before_delete_post', __NAMESPACE__ . '\\delete_view', 10, 3);
function delete_view($post_id)
{
    $post_type = get_post_type($post_id);

    if ($post_type == 'views') {
        $omneo_id = get_field('omneo_id', $post_id);

        $args = [
            'api_request' => 'content/views/' . $omneo_id,
            'verb' => 'delete'
        ];

        $response = Core\send_request($args);

        if (isset($response['error']) || !isset($response['data'])) {
            // API Error
            if (isset($response['error']['message'])) {
                add_action('admin_notices', function ($response) {
                    echo "<div class='error'> <p>" . $response['error']['message'] . "</p></div>";
                });
                die($response['error']['message']);
            } else {
                die('Omneo API Error');
            }

        }
    }
}


/**
 * Load assets
 */
function load_assets()
{
    if((isset($_GET['post']) && get_post_type($_GET['post']) == 'views') || (isset($_GET['post_type']) && $_GET['post_type'] == 'views')) {
        wp_enqueue_script('views', plugins_url() . DS . 'omneo-views' . DS . 'assets' . DS . 'main.js');
    }

    // Content item
    if((isset($_GET['post']) && get_post_type($_GET['post']) == 'content_items') || (isset($_GET['post_type']) && $_GET['post_type'] == 'content_items')) {
        wp_enqueue_script('angular', plugins_url() . DS . 'omneo-core' . DS . 'assets' . DS . 'angular.min.js');
        wp_enqueue_script('views', plugins_url() . DS . 'omneo-views' . DS . 'assets' . DS . 'content-item.js');
    }
}
load_assets();

/**
 * Views ajax request
 */
add_action('wp_ajax_views_send_request', __NAMESPACE__ . '\\views_send_request');
function views_send_request()
{
    $data = json_decode(stripslashes($_GET['data']), true);
    $response = Core\send_request($data);
    $json = json_encode($response);
    echo $json;
    exit;
}






// Setup metabox
add_action('load-post.php', __NAMESPACE__ . '\\omneo_views_post_meta_box_setup');
add_action('load-post-new.php', __NAMESPACE__ . '\\omneo_views_post_meta_box_setup');
function omneo_views_post_meta_box_setup()
{
    add_meta_box(
        'content-item-views',           // Unique ID
        esc_html__('Views', 'views'),   // Title
        __NAMESPACE__ . '\\content_item_views',       // Callback function
        'content_items',                // Admin page (or post type)
        'side',                       // Context
        'default'                       // Priority
    );
}


/**
 * Rules view
 */
function content_item_views()
{
    // Load metabox
    require_once(__PATH() . '/lib/content-item.php');
}