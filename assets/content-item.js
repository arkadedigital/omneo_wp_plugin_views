angular.module('views', []);
angular.element(document).ready(function () {
    angular.bootstrap(document.getElementById('ng-views'), ['views']);
});

angular.module('views').controller('ViewsCtrl', function ($scope, viewsService, $timeout) {
    $scope.views = [];
    $scope.selected_views = [];

    // Wait for $scope.content_item_id to initialize
    $timeout(function () {
        // Get content item views
        if ($scope.content_item_id) {
            viewsService.getContentViews({content_item_id: $scope.content_item_id}).then(function (data) {

                _.each(data, function (content_item_view) {
                    var view = _.find($scope.views, function (v) {
                        return v.id == content_item_view.view_id;
                    });

                    if(view) {
                        content_item_view.title = view.title;

                        // Default action to 'add' if lookbook or gallery content type
                        if($scope.content_type === 'lookbooks' || $scope.content_type === 'gallery') {
                            content_item_view.action = 'add';
                        }

                    } else {
                        content_item_view.title = 'View does not exist';
                    }
                });

                $scope.selected_views = data;
                $scope.is_loaded = true;
            });
        } else {
            $scope.is_loaded = true;
        }
    });

    $scope.add_selected_view = function (view_id) {

        // If added already, don't add again
        var view_exists = _.find($scope.selected_views, function (selected_view) {
            return selected_view.view_id == view_id;
        });
        if (view_exists) {
            view_exists.action = '';
            return false;
        }


        var view = _.find($scope.views, function (v) {
            return v.id == view_id;
        });
        $scope.selected_views.push({id: view.id, view_id: view_id, title: view.title, action: 'add'});
    };

    $scope.delete_view = function (view) {
        if (view.action !== 'add') {
            view.action = 'delete';
        } else {
            $scope.selected_views = _.without($scope.selected_views, view);
        }
    };
});


/**
 * Views service
 */
angular.module('views').factory('viewsService', function ($http, $q) {
    return {
        /**
         * Get views
         * @returns {*}
         */
        getContentViews: function (args) {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'views_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'content/viewitems',
                        data: args
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        },

        /**
         * Get content segments
         * @returns {*}
         */
        getContentSegments: function () {
            return $http({
                url: ajaxurl,
                method: 'POST',
                params: {
                    action: 'segments_send_request',
                    data: {
                        verb: 'get',
                        api_request: 'contentsegments'
                    }
                }
            }).then(function (response) {
                if (typeof response.data.error !== 'undefined') {
                    return $q.reject(response.data.error.message);
                } else if (response.data === 'null') {
                    return $q.reject('Omneo API Error');
                }

                return response.data.data;
            });
        }
    };
});